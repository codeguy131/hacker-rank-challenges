
/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

fn plus_minus(arr: &[i32]) {
    let mut r_pos: f32 = 0.0;
    let mut r_neg: f32 = 0.0;
    let mut r_zeros: f32 = 0.0;
    for i in &arr[..] {
        
        if i.is_positive() {
            r_pos += 1.0;
        } else if i.is_negative() {
            r_neg += 1.0;
        } else if *i == 0 {
            r_zeros += 1.0;
        }
    }
    let r_pos = r_pos / arr.len() as f32;
    let r_neg = r_neg / arr.len() as f32;
    let r_zeros = r_zeros / arr.len() as f32;
    println!("{r_pos}");
    println!("{r_neg}");
    println!("{r_zeros}");
}

fn main() {

    let arr = [-4, 3, -9, 0, 4, 1];

    println!("{:?}", arr);
    plus_minus(&arr);
}