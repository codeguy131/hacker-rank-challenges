#!/bin/python3

import math
import os
import random
import re
import sys
import time

#
# Complete the 'timeConversion' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def timeConversion(s):
    # Write your code here
    if str(s).endswith("M" or "m"):
        res = int(str(s).strip(":"))
        print(res)
        return str(res)



if __name__ == '__main__':

    s = input()

    result = timeConversion(s)
    print(result)
