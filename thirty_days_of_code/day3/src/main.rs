/*
 Objective
In this challenge, we learn about conditional statements. Check out the Tutorial tab for learning materials and an instructional video.

Task
Given an integer n,

, perform the following conditional actions:

    If n

is odd, print Weird
If n
is even and in the inclusive range of 2 to 5
, print Not Weird
If n
is even and in the inclusive range of 6 to 20
, print Weird
If n
is even and greater than 20, print Not Weird
 */


fn day3(n: i32) {

    if (n % 2) != 0 {
        println!("Weird")
    } else if (n % 2) == 0 && std::ops::RangeInclusive::new(2, 5).contains(&n) {
        println!("Not Weird")
    } else if (n % 2) == 0 && std::ops::RangeInclusive::new(6, 20).contains(&n) {
        println!("Weird")
    } else if (n % 2) == 0 && n > 20 {
        println!("Not Weird")
    } 

}
fn main() {
    let mut n = String::new();
    std::io::stdin().read_line(&mut n).unwrap();
    let n = n.trim();
    let n: i32 = n.parse::<i32>().unwrap();
    day3(n);
}
