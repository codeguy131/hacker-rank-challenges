fn hello() {
    let mut buff = String::new();
    std::io::stdin().read_line(&mut buff).unwrap();
    println!("Hello, World.");
    println!("{}", buff);
}
fn main() {
    hello()
}
