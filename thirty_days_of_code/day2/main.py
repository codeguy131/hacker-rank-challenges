#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'solve' function below.
#
# The function accepts following parameters:
#  1. DOUBLE meal_cost
#  2. INTEGER tip_percent
#  3. INTEGER tax_percent
#


def solve(meal_cost, tip_percent, tax_percent):
    tip = float(meal_cost) * (tip_percent / 100)
    tax = float(meal_cost) * (tax_percent / 100)
    total = float(meal_cost) + tip + tax
    rounded = round(float(total))
    print(rounded)


if __name__ == "__main__":
    meal_cost = float(12.00)

    tip_percent = int(20)

    tax_percent = int(8)

    solve(meal_cost, tip_percent, tax_percent)
