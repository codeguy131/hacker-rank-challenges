
/*
 * Complete the 'aVeryBigSum' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts LONG_INTEGER_ARRAY ar as parameter.
 */

fn aVeryBigSum(ar: &[i64]) -> i64 {
    ar.iter().fold(0, |x, y| -> i64 {x + y})
}

fn main() {

    let ar = [1, 3, 6];

    let result = aVeryBigSum(&ar);
    println!("{:?}", result);
}