/*
 * Complete the 'compareTriplets' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY a
 *  2. INTEGER_ARRAY b
 */

use std::cmp::Ordering;

fn compareTriplets(a: &[i32], b: &[i32]) -> Vec<i32> {
    let mut points = vec![0; 2];

    for i in a.iter().zip(b.iter()) {
        match i.0.cmp(i.1) {
            Ordering::Greater => points[0] += 1,
            Ordering::Less => points[1] += 1,
            _ => ()
        }
    }

    return points;
}

fn main() {
    let a = &[5, 6, 7];
    let b = &[3, 6, 10];
    let res = compareTriplets(a, b);
    println!("TEST DATA: {:?}", (a, b));
    println!("RESULT: {:?}", res);
}
