use std::{io::{self, BufRead}, ops::Sub, slice::SliceIndex};
/*
 * Complete the 'staircase' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

fn staircase(n: i32) {
    for count in (0..(n as usize)).rev() {
        let mut out = (0..n).map(|_| ' ').collect::<String>();
        for i in count..(n as usize) {
            out.insert(i, '#');
        }
        println!("{out}");
    }
}

fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();

    let n = stdin_iterator
        .next()
        .unwrap()
        .unwrap()
        .trim()
        .parse::<i32>()
        .unwrap();

    staircase(n);
}
